package cl.bennu.parser.dto;

import java.io.Serializable;

/**
 * Created by liot on 8/6/14.
 */
public class BaseDTO implements Serializable {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
