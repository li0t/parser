package cl.bennu.parser.dto;

/**
 * Created by liot on 8/8/14.
 */
public class ClienteDTO extends BaseDTO {
    private String dv;

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }
}
