package cl.bennu.parser.enums;

/**
 * Created by bennu on 13-08-14.
 */
public enum DBTypes {
    MYSQL(1, "MYSQL"),
    POSTGRES(2, "POSTGRES");

    private final int id;
    private final String label;

    private DBTypes(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public static DBTypes getEnum(int id) {
        for (DBTypes e : DBTypes.values()) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return label;
    }
}