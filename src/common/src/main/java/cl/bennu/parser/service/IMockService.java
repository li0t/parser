package cl.bennu.parser.service;

import cl.bennu.parser.dto.*;
import cl.bennu.parser.enums.ServicesNames;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by liot on 8/25/14.
 */
@Local
public interface IMockService {

    List<RegionDTO> getRegiones();

    List<ProvinciaDTO> getProvincias(int regId);

    List<ComunaDTO> getComunas(int proId);

    List<DireccionDTO> getDirecciones(int rut, int com, int pro, int reg);

    String parse(BaseDTO dto, ServicesNames servicesNames);

    String parse(List<DireccionDTO> list, ServicesNames servicesNames);

}
