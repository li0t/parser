package cl.bennu.parser.dto;

import java.io.Serializable;

/**
 * Created by liot on 7/29/14.
 */
public class DireccionDTO implements Serializable {
    private int id;
    private String name;
    private int numdir;
    private String nomcom;
    private String nompro;
    private String nomreg;
    private String nomtipo;


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumdir(int numdir) {
        this.numdir = numdir;
    }

    public void setNompro(String nompro) {
        this.nompro = nompro;
    }

    public void setNomcom(String nomcom) {
        this.nomcom = nomcom;
    }

    public void setNomreg(String nomreg) {
        this.nomreg = nomreg;
    }

    public void setNomtipo(String nomtipo) {
        this.nomtipo = nomtipo;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNumdir() {
        return numdir;
    }

    public String getNomcom() {
        return nomcom;
    }

    public String getNompro() {
        return nompro;
    }

    public String getNomreg() {
        return nomreg;
    }

    public String getNomtipo() {
        return nomtipo;
    }
}
