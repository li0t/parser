package cl.bennu.parser.enums;

import java.io.Serializable;

/**
 * Created by liot on 8/11/14.
 */
public enum ServicesNames implements Serializable {
    XML(1, "XML"),
    JSON(2, "JSON");

    private final int id;
    private final String label;

    private ServicesNames(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public static ServicesNames getEnum(int id) {
        for (ServicesNames e : ServicesNames.values()) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }

    public static ServicesNames getEnum(String name) {
        for (ServicesNames e : ServicesNames.values()) {
            if (e.getLabel().equals(name)) {
                return e;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return label;
    }
}

