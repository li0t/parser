package cl.bennu.parser.dto;

/**
<<<<<<< HEAD
 * Created by liot on 8/27/14.
 */
public class ConsultaDTO {
    private int rut;
    private int idReg;
    private int idPro;
    private int idCom;

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public int getIdPro() {
        return idPro;
    }

    public void setIdPro(int idPro) {
        this.idPro = idPro;
    }

    public int getIdReg() {
        return idReg;
    }

    public void setIdReg(int idReg) {
        this.idReg = idReg;
    }

    public int getIdCom() {
        return idCom;
    }

    public void setIdCom(int idCom) {
        this.idCom = idCom;
    }
}
