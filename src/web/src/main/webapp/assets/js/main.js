/**
 * Created by liot on 8/15/14.
 */
function isValid() {

    return $("#regionSelect").val() !== ""
        || ( $("#rutCliente").val() !== ""
            && ( $("#rutCliente").hasClass
            ("campo-requerido") === false ) )
}

function checkResult(success, obj) {
    if (success) {
        obj.removeClass("campo-requerido");
        console.log("bien");
    } else {
        obj.addClass("campo-requerido");
        console.log("mal");
    }
}

function renderjson() {
    var reg = $('#regionSelect'),
        pro = $('#provinciaSelect'),
        com = $('#comunaSelect'),
        idReg = reg.val(),
        idPro = -1;
    if (pro.val() != null)
        idPro = pro.val();
    console.log("idReg: " + idReg + " " + "idPro: " + idPro + " idCom: " + com.val());
    $.getJSON('/parser/JSON', {"idReg": idReg, "idPro": idPro }, function (data) {
        $.each(data.homeForm.provincias, function (i, item) {
            $('<option>').val(item.id).text(item.name).appendTo(pro);
        });
        $.each(data.homeForm.comunas, function (i, item) {
            $('<option>').val(item.id).text(item.name).appendTo(com);
        });
    });
}

function resetRut(id) {
    $(id).removeClass("campo-requerido");
    $(id).val(null);
}

function resetSelect(id) {
    $(id).find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('whatever')
        .select2({
            width: '100%',
            placeholder: "Seleccione"
        });
}

function initSelect(id) {
    $(id).select2({
        width: '100%',
        placeholder: "Seleccione"
    });
}
$(document).ready(function () {

    $("#xml").attr('checked', true);
    initSelect("#regionSelect");
    initSelect("#provinciaSelect");
    initSelect("#comunaSelect");


    $("#regionSelect").on('change', function () {
        resetRut("#rutCliente");
        resetSelect("#provinciaSelect");
        resetSelect("#comunaSelect");
        renderjson();
    });

    $("#provinciaSelect").on('change', function () {
        resetSelect("#comunaSelect");
        renderjson();
    });


    $("#rutCliente")
        .check('rut', {success: checkResult})
        .on('keydown', function () {
            $('#regionSelect').select2('val', $('.select2 option:eq(1)').val());
            resetSelect("#provinciaSelect");
            resetSelect("#comunaSelect");
        });

    $("#aceptar")
        .on('click', function () {
            if (isValid()) {

                var form = $("#form"),
                    idReg = $('#regionSelect').val(),
                    idPro = $('#provinciaSelect').val(),
                    idCom = $('#comunaSelect').val(),
                    rut = $("#rutCliente").val();

                if (idReg !== "") {
                    rut = -1;
                    if (idPro === "") {
                        idPro = -1;
                        idCom = -1;
                    } else {
                        idReg = -1;
                        if (idCom === "") {
                            idCom = -1;
                        } else {
                            idPro = -1;
                        }
                    }
                } else {
                    idReg = -1;
                    idPro = -1;
                    idCom = -1;
                    rut = rut.substring(0, rut.length - 1);
                }

                form.attr("action", "getDirecciones/" + rut + "/" + idReg + "/" + idPro + "/" + idCom);
                form.submit();

                console.log("envié a server");
            } else {
                console.log("Campos requeridos");

            }
        })
        .bind('contextmenu keydown click', function () {
            event.preventDefault();
            console.log(event.type);
        })

});
