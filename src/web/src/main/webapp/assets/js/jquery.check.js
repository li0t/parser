/**
 * Created by liot on 7/2/14.
 */
(function ($, window, document) {

    $.fn.check = function (method) {
        var self = this,
            chk = {
                options: $.extend({
                    'validate': "",
                    'blocked': false,
                    'success': function () {
                    }
                }, Array.prototype.slice.call(arguments, 1)[0]),

                rut: function () {
                    return self.each(function () {
                        self.on("blur", function () {
                            chk.options.success((isValid(self.val())),self);
                            /*var expression = /^([1-9][0-9]?.[0-9]{3}.[0-9]{3}-[0-9k]{1}|[1-9][0-9][0-9]{3}[0-9]{3}-?[0-9k]{1})$/,
                             val = self.val();
                             if (expression.test(val) || val == "") {
                             chk.options.success(self, true);
                             } else {
                             chk.options.success(self, false);
                             }*/
                        });
                    });
                },

                email: function () {
                    return self.each(function () {
                        self.on("blur", function () {
                            var val = self.val(),
                                expression = /^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}$/i;
                            if (expression.test(val) || val === "") {
                                chk.options.success(true,self);
                            } else {
                                chk.options.success(false,self);
                            }
                        });

                    });
                },

                numeric: function () {
                    return self.each(function () {
                        self.on("keydown", function (event) {
                            var which = event.which,
                                isValid = true;
                            if (!(which >= 48 && which <= 57) && !(which >= 96 && which <= 105) && !(which >= 8 && which <= 9)
                                && !(which >= 35 && which <= 37) && !(which === 39) && !(which === 46)) {
                                event.preventDefault();
                                isValid = false;
                            }
                            chk.options.success(isValid,self);
                        });
                    });
                }
            };

        if (chk.options.blocked) {
            self.bind('cut copy paste', function () {
                console.log("No puedes hacer " + event.type);
                event.preventDefault();
            });
        }

        if (chk[method]) {
            return chk[method].call();
        }

        function isValid(rut) {
            var valid = true;
            if (rut !== null && rut.length >= 2) {
                var strDv = rut.charAt(rut.length - 1).toUpperCase(),
                    rut = rut.substring(0, rut.length - 1),
                    intSuma = 0;
                if (!isNaN(rut)) {
                    var lngRutValue = Number(rut),
                        intLar = rut.length;
                    while (intLar > 0) {
                        var intBase = parseFloat(Math.pow(10, (intLar - 1))),
                            intMul = (( intLar - 1 ) % 6) + 2,
                            intDM = parseInt(lngRutValue / intBase),
                            intSuma = intSuma + (intDM * intMul);
                        lngRutValue = lngRutValue - (intDM * intBase);
                        intLar--;
                    }
                    var intDv = 11 - intSuma % 11,
                        Dv = (intDv === 10 ? 'K' : (intDv === 11 ? '0' : intDv + ''));
                    valid = strDv === Dv;
                } else {
                    valid=false;
                }
            }
            return valid;
        }
    };

})(jQuery, window, document);