<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Parser</title>

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/semantic-ui/0.19.0/css/semantic.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/select2/3.4.8/select2.css">

    <style>
        body {
            font-family: 'Open Sans', sans-serif !important;
            font-size: 12px;
        }

        .btn-aceptar {
            margin-top: 30px !important;
        }

        .ui.header.header-parser {
            text-transform: uppercase;
        }

        input.campo-requerido, select.campo-requerido {
            border: 1px solid #CC0000;
        }

    </style>

</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<div class="ui column grid">
    <div class="column">
        <form id="form" method="get">
            <div class="ui fluid form segment">
                <h3 class="ui header header-parser">Parser</h3>

                <div class="three fields">
                    <div class="field">
                        <label>Región</label>
                        <form:select path="homeForm.idReg" name="regionParser"
                                     id="regionSelect" placeholderOption="true">
                            <option></option>
                            <form:options items="${homeForm.regiones}" itemValue="id" itemLabel="name"/>
                        </form:select>
                    </div>
                    <div class="field">
                        <label>Provincia</label>
                        <form:select path="homeForm.idPro" name="provinciaParser"
                                     id="provinciaSelect" placeholderOption="true">
                            <option></option>
                        </form:select>
                    </div>
                    <div class="field">
                        <label>Comuna</label>
                        <form:select path="homeForm.idCom" name="comunaParser"
                                     id="comunaSelect" placeholderOption="true">
                            <option></option>
                        </form:select>
                    </div>
                </div>
                <div class="three fields">
                    <div class="field">
                        <label for="">Rut Cliente</label>

                        <div class="ui tiny icon input">
                            <input type="text" placeholder="Buscar Rut Cliente..." id="rutCliente">
                            <i class="search icon"></i>
                        </div>
                        <label for="">Tipo Archivo</label>

                        <div>
                            <input type="radio" name="type" id="xml" value="xml">XML <br>
                            <input type="radio" name="type" id="json" value="json">JSON <br>
                            <br/>
                        </div>
                    </div>
                    <div class="field">
                        <input class="tiny ui submit button btn-aceptar" id="aceptar" value="Enviar" autocomplete="off">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery-1.11.0.min.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/select2.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/select2_locale_es.js"></script>
<script src="//cdn.jsdelivr.net/semantic-ui/0.19.0/javascript/semantic.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/jquery.check.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/main.js"></script>


<script>

</script>
</body>
</html>