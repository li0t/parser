package cl.bennu.parser.web.form;

import cl.bennu.parser.dto.ComunaDTO;
import cl.bennu.parser.dto.DireccionDTO;
import cl.bennu.parser.dto.ProvinciaDTO;
import cl.bennu.parser.dto.RegionDTO;

import java.util.List;

/**
 * Created by liot on 8/13/14.
 */
public class HomeForm {
    private String output;
    private int idReg;
    private int idPro;
    private int idCom;
    private int rut;
    private List<RegionDTO> regiones;
    private List<ProvinciaDTO> provincias;
    private List<ComunaDTO> comunas;
    private List<DireccionDTO> direcciones;

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutput() {
        return output;
    }

    public List<RegionDTO> getRegiones() {
        return regiones;
    }

    public void setRegiones(List<RegionDTO> regiones) {
        this.regiones = regiones;
    }

    public List<ProvinciaDTO> getProvincias() {
        return provincias;
    }

    public void setProvincias(List<ProvinciaDTO> provincias) {
        this.provincias = provincias;
    }

    public List<ComunaDTO> getComunas() {
        return comunas;
    }

    public void setComunas(List<ComunaDTO> comunas) {
        this.comunas = comunas;
    }

    public List<DireccionDTO> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(List<DireccionDTO> direcciones) {
        this.direcciones = direcciones;
    }

    public int getIdReg() {
        return idReg;
    }

    public void setIdReg(int idReg) {
        this.idReg = idReg;
    }

    public int getIdPro() {
        return idPro;
    }

    public void setIdPro(int idPro) {
        this.idPro = idPro;
    }

    public int getIdCom() {
        return idCom;
    }

    public void setIdCom(int idCom) {
        this.idCom = idCom;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }
}
