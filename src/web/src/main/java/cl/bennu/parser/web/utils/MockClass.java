package cl.bennu.parser.web.utils;

import cl.bennu.parser.dto.ConsultaDTO;
import cl.bennu.parser.dto.DireccionDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liot on 8/27/14.
 */
public class MockClass {

    public MockClass() {

    }

    public List<DireccionDTO> listMaker(ConsultaDTO cns) {
        DireccionDTO a, b, c;
        a = new DireccionDTO();
        b = new DireccionDTO();
        c = new DireccionDTO();

        a.setName("A");

        b.setName("B");

        c.setName("C");

        List<DireccionDTO> list = new ArrayList<DireccionDTO>();

        list.add(a);
        list.add(b);
        list.add(c);

        return list;
    }
}
