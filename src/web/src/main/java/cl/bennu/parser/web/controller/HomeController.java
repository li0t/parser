package cl.bennu.parser.web.controller;


import cl.bennu.parser.dto.ComunaDTO;
import cl.bennu.parser.dto.DireccionDTO;
import cl.bennu.parser.dto.ProvinciaDTO;
import cl.bennu.parser.enums.ServicesNames;
import cl.bennu.parser.service.IMockService;
import cl.bennu.parser.web.form.HomeForm;
import cl.bennu.parser.web.utils.MockClass;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by bennu on 13-08-14.
 */


@Controller
public class HomeController {

    @Autowired
    private IMockService mockService;


    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String doGet(HomeForm homeForm) {
        homeForm.setRegiones(mockService.getRegiones());
        return "parser";
    }

    //Descarga un archivo con las direcciones asociadas a parametros.
    @RequestMapping(value = "/getDirecciones/{rut}/{idReg}/{idPro}/{idCom}", method = RequestMethod.GET)
    public void getDirecciones(
            @PathVariable("rut") int rut,
            @PathVariable("idReg") int idReg,
            @PathVariable("idPro") int idPro,
            @PathVariable("idCom") int idCom,
            @ModelAttribute("type") String type,
            HttpServletResponse response) {
        try {

//            ConsultaDTO cns = new ConsultaDTO();
//            cns.setRut(rut);
//            cns.setIdReg(idReg);
//            cns.setIdPro(idPro);
//            cns.setIdCom(idCom);

            List<DireccionDTO> list = mockService.getDirecciones(rut,idCom,idPro,idReg);

            if( list.size() != 0) {

                String parsed = mockService.parse(list, ServicesNames.getEnum(type.toUpperCase()));
                InputStream is = new ByteArrayInputStream(parsed.getBytes());


                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=Direcciones-" + type);
                IOUtils.copy(is, response.getOutputStream());
                response.flushBuffer();

            } else {
                String s = "'There is no data associated'";
                InputStream is = new ByteArrayInputStream(s.getBytes());

                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=NotFound");
                IOUtils.copy(is, response.getOutputStream());
                response.flushBuffer();
            }

        } catch (IOException ex) {
            System.err.println("Error writing file to output stream. Filename was '" + type + "'");
            throw new RuntimeException("IOError writing file to output stream");
        }
    }


    @RequestMapping(value = "/JSON", method = RequestMethod.GET)
    public String getData(
            @RequestParam("idReg") int idReg,
            @RequestParam("idPro") int idPro,
            @ModelAttribute("homeForm") HomeForm homeForm){
        if(idPro ==-1){
            homeForm.setProvincias(mockService.getProvincias(idReg));
            homeForm.setComunas(new ArrayList<ComunaDTO>());
        }
        else{
            homeForm.setProvincias(new ArrayList<ProvinciaDTO>());
            homeForm.setComunas(mockService.getComunas(idPro));
        }

        return "jsonView";
    }


}

