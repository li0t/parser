package cl.bennu.parser.persistencia.factory.iface;

/**
 * Created by bennu on 12-08-14.
 */
public interface IFactory {

    IRegionDAO getRegionDAO();

    IProvinciaDAO getProvinciaDAO();

    IComunaDAO getComunaDAO();

    IDireccionDAO getDireccionDAO();

}
