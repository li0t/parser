package cl.bennu.parser.business.impls;


import cl.bennu.parser.business.ifaces.IParseBO;
import cl.bennu.parser.dto.BaseDTO;
import cl.bennu.parser.dto.DireccionDTO;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

import java.util.List;


/**
 * Created by liot on 8/12/14.
 */
public class ParseJSON implements IParseBO {

    private XStream xStream = new XStream(new JettisonMappedXmlDriver());

    @Override
    public String getName()  {
        return "ParseJSON";
    }

    @Override
    public String parse(BaseDTO dto) {
        return xStream.toXML(dto);
    }

    @Override
    public String parse(List<DireccionDTO> list){
        return  xStream.toXML(list);
    }

}