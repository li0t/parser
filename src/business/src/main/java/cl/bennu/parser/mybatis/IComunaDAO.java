package cl.bennu.parser.mybatis;

import cl.bennu.parser.dto.ComunaDTO;

import java.util.List;

/**
 * Created by liot on 7/29/14.
 */
public interface IComunaDAO {
    List<ComunaDTO> getComunas(int idProvincia);
}
