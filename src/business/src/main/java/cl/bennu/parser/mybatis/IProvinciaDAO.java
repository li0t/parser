package cl.bennu.parser.mybatis;

import cl.bennu.parser.dto.ProvinciaDTO;

import java.util.List;

/**
 * Created by liot on 7/29/14.
 */
public interface IProvinciaDAO {
    List<ProvinciaDTO> getProvincias(int idRegion);
}
