package cl.bennu.parser.persistencia.factory.impls.mysql;


import cl.bennu.parser.dto.RegionDTO;
import cl.bennu.parser.enums.DBTypes;
import cl.bennu.parser.persistencia.factory.iface.IRegionDAO;
import cl.bennu.parser.persistencia.utils.ConectorJDBC;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bennu on 12-08-14.
 */
public class RegionDAO  implements IRegionDAO {
    ConectorJDBC conector;
    Connection connection;
    Statement statement;
    ResultSet res;
    String consulta = "SELECT * FROM region";


    public RegionDAO(DBTypes dbType) {
        conector = new ConectorJDBC(get(),dbType);
        connection = conector.getConexion();
    }

    @Override
    public String get() {
        return "com.mysql.jdbc.Driver";
    }

    @Override
    public List<RegionDTO> getRegiones() {
        List<RegionDTO> list = new ArrayList();

        try {
            statement = connection.createStatement();
            res = statement.executeQuery(consulta);
            while (res.next()) {
                RegionDTO regionDTO = new RegionDTO();
                regionDTO.setId(res.getInt(1));
                regionDTO.setName(res.getString(2));
                list.add(regionDTO);
            }
            res.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return list;
    }

}
