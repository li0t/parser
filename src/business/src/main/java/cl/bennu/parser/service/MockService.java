package cl.bennu.parser.service;

import cl.bennu.parser.business.ifaces.ILocalidadBO;
import cl.bennu.parser.business.ifaces.IParseBO;
import cl.bennu.parser.dto.*;
import cl.bennu.parser.enums.ServicesNames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.List;
import java.util.Map;

/**
 * Created by liot on 8/25/14.
 */
@Stateless(name = "MockService")
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class MockService implements IMockService {

    @Autowired
    ILocalidadBO localidadBO;


    @Autowired
    Map<String, IParseBO> parseMap;

    public ILocalidadBO getLocalidadBO() {
        return localidadBO;
    }

    public void setLocalidadBO(ILocalidadBO localidadBO) {
        this.localidadBO = localidadBO;
    }

    public Map<String, IParseBO> getParseMap() {
        return parseMap;
    }

    public void setParseMap(Map<String, IParseBO> parseMap) {
        this.parseMap = parseMap;
    }

    @Override
    public List<RegionDTO> getRegiones() {
        return localidadBO.getRegiones();
    }

    @Override
    public List<ProvinciaDTO> getProvincias(int regId) {

            return localidadBO.getProvincias(regId);
    }

    @Override
    public List<ComunaDTO> getComunas(int proId) {

        return  localidadBO.getComunas(proId);
    }

    @Override
    public List<DireccionDTO> getDirecciones(int rut, int com, int pro, int reg) {
        return localidadBO.getDirecciones(rut, com, pro, reg);
    }

    @Override
    public String parse(BaseDTO dto, ServicesNames serviceName) {

         return parseMap.get(serviceName.getLabel()).parse(dto);
    }

    @Override
    public String parse(List<DireccionDTO> list, ServicesNames serviceName) {
       ;
        return  parseMap.get(serviceName.getLabel().toLowerCase()).parse(list);
    }
}
