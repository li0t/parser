package cl.bennu.parser.business.impls;


import cl.bennu.parser.business.ifaces.IParseBO;
import cl.bennu.parser.dto.BaseDTO;
import cl.bennu.parser.dto.DireccionDTO;
import com.thoughtworks.xstream.XStream;

import java.util.List;

/**
 * Created by liot on 8/8/14.
 */
public class ParseXML implements IParseBO {

    private XStream xStream = new XStream();

    @Override
    public String getName()  {
        return "ParseXML";
    }

    @Override
    public String parse(BaseDTO dto) {
        return xStream.toXML(dto);
    }

    @Override
    public String parse(List<DireccionDTO> list){
        return  xStream.toXML(list);
    }

}