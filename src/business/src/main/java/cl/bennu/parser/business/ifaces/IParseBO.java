package cl.bennu.parser.business.ifaces;


import cl.bennu.parser.dto.BaseDTO;
import cl.bennu.parser.dto.DireccionDTO;

import java.util.List;


/**
 * Created by liot on 8/4/14.
 */

public interface IParseBO {

    String getName();

    String parse(BaseDTO dto);

    String parse(List<DireccionDTO> list);


}
