package cl.bennu.parser.mybatis;

import cl.bennu.parser.dto.DireccionDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by liot on 7/29/14.
 */
public interface IDireccionDAO {

    List<DireccionDTO> getDirecciones(@Param("rut") int rut, @Param("com") int com, @Param("pro") int pro, @Param("reg") int reg);


}
