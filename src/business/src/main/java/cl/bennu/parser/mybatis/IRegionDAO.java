package cl.bennu.parser.mybatis;

import cl.bennu.parser.dto.RegionDTO;

import java.util.List;

/**
 * Created by liot on 7/29/14.
 */
public interface IRegionDAO {
    List<RegionDTO> getRegiones();
}
