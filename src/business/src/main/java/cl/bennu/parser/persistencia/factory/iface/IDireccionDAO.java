package cl.bennu.parser.persistencia.factory.iface;

import cl.bennu.parser.dto.DireccionDTO;
import java.util.List;

/**
 * Created by bennu on 25-08-14.
 */
public interface IDireccionDAO {
    String get();

    List<DireccionDTO> getDirecciones(Object form);
}
