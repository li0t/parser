package cl.bennu.parser.business.impls;

import cl.bennu.parser.business.ifaces.ILocalidadBO;
import cl.bennu.parser.dto.ComunaDTO;
import cl.bennu.parser.dto.DireccionDTO;
import cl.bennu.parser.dto.ProvinciaDTO;
import cl.bennu.parser.dto.RegionDTO;
import cl.bennu.parser.mybatis.IComunaDAO;
import cl.bennu.parser.mybatis.IDireccionDAO;
import cl.bennu.parser.mybatis.IProvinciaDAO;
import cl.bennu.parser.mybatis.IRegionDAO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by bennu on 29-07-14.
 */
public class LocalidadBO implements ILocalidadBO {


    @Autowired
    private IRegionDAO regionDAO;
    @Autowired
    private IProvinciaDAO provinciaDAO;
    @Autowired
    private IComunaDAO comunaDAO;
    @Autowired
    private IDireccionDAO direccionDAO;

    public void setRegionDAO(IRegionDAO regionDAO) {
        this.regionDAO = regionDAO;
    }

    public void setComunaDAO(IComunaDAO comunaDAO) {
        this.comunaDAO = comunaDAO;
    }

    public void setProvinciaDAO(IProvinciaDAO provinciaDAO) {
        this.provinciaDAO = provinciaDAO;

    }
    public void setDireccionDAO(IDireccionDAO direccionDAO) {
        this.direccionDAO = direccionDAO;
    }
    public List<RegionDTO> getRegiones() {
        List<RegionDTO> listaRegiones;

        listaRegiones = this.regionDAO.getRegiones();

        return listaRegiones;
    }

    public List<ProvinciaDTO> getProvincias(int idRegion) {
        List<ProvinciaDTO> listaProvincias;

        listaProvincias = this.provinciaDAO.getProvincias(idRegion);

        return listaProvincias;
    }

    public List<ComunaDTO> getComunas(int idProvincia) {
        List<ComunaDTO> listaComunas;

        listaComunas = this.comunaDAO.getComunas(idProvincia);

        return listaComunas;
    }

    public List<DireccionDTO> getDirecciones(int rut, int com, int pro, int reg){
        List<DireccionDTO> listaDirecciones;
        listaDirecciones = this.direccionDAO.getDirecciones(rut, com, pro,reg);
        return listaDirecciones;
    }


}
