package cl.bennu.parser.persistencia.factory.iface;

/**
 * Created by bennu on 12-08-14.
 */
public interface ITypedDAO<T> {

    T get();
}
