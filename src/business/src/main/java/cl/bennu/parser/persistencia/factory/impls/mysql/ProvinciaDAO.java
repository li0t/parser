package cl.bennu.parser.persistencia.factory.impls.mysql;

import cl.bennu.parser.dto.ProvinciaDTO;
import cl.bennu.parser.enums.DBTypes;
import cl.bennu.parser.persistencia.factory.iface.IProvinciaDAO;
import cl.bennu.parser.persistencia.utils.ConectorJDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bennu on 25-08-14.
 */
public class ProvinciaDAO implements IProvinciaDAO {
    ConectorJDBC conector;
    Connection connection;
    PreparedStatement statement;
    ResultSet res;
    String consulta = "SELECT * FROM provincia where idRegPro=?";


    public ProvinciaDAO(DBTypes dbType) {
        conector = new ConectorJDBC(get(),dbType);
        connection = conector.getConexion();
    }

    @Override
    public String get() {
        return "com.mysql.jdbc.Driver";
    }

    @Override
    public List<ProvinciaDTO> getProvincias(int regId) {
        List<ProvinciaDTO> list = new ArrayList();

        try {
            statement = connection.prepareStatement(consulta);
            statement.setInt(1,regId);
            res = statement.executeQuery();
            while (res.next()) {
                ProvinciaDTO provinciaDTO = new ProvinciaDTO();
                provinciaDTO.setId(res.getInt(1));
                provinciaDTO.setName(res.getString(2));
                list.add(provinciaDTO);
            }
            res.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return list;
    }
}
