package cl.bennu.parser.persistencia.factory.impls;

import cl.bennu.parser.enums.DBTypes;
import cl.bennu.parser.persistencia.factory.abstracts.AbstractFactoryJDBC;
import cl.bennu.parser.persistencia.factory.iface.*;

/**
 * Created by bennu on 12-08-14.
 */
public class Factory extends AbstractFactoryJDBC implements IFactory {
    private DBTypes dbType= DBTypes.MYSQL;
    private String driver;

    public Factory(DBTypes dbType) {
    }

    @Override
    public IRegionDAO getRegionDAO() {
        IRegionDAO idao = null;
        if (DBTypes.POSTGRES == dbType) {
            idao = new cl.bennu.parser.persistencia.factory.impls.postgres.RegionDAO(dbType);
        } else if (DBTypes.MYSQL == dbType) {
            idao = new cl.bennu.parser.persistencia.factory.impls.mysql.RegionDAO(dbType);
        }
        return idao;
    }
    @Override
    public IProvinciaDAO getProvinciaDAO() {
        IProvinciaDAO idao = null;
        if (DBTypes.POSTGRES == dbType) {
            idao = new cl.bennu.parser.persistencia.factory.impls.postgres.ProvinciaDAO(dbType);
        } else if (DBTypes.MYSQL == dbType) {
            idao = new cl.bennu.parser.persistencia.factory.impls.mysql.ProvinciaDAO(dbType);
        }
        return idao;
    }
    @Override
    public IComunaDAO getComunaDAO() {
        IComunaDAO idao = null;
        if (DBTypes.POSTGRES == dbType) {
            idao = new cl.bennu.parser.persistencia.factory.impls.postgres.ComunaDAO(dbType);
        } else {
            idao = new cl.bennu.parser.persistencia.factory.impls.mysql.ComunaDAO(dbType);        }
        return idao;
    }
    @Override
    public IDireccionDAO getDireccionDAO() {
        IDireccionDAO idao = null;
        if (DBTypes.POSTGRES == dbType) {
            idao = new cl.bennu.parser.persistencia.factory.impls.postgres.DireccionDAO(dbType);
        } else {
            idao = new cl.bennu.parser.persistencia.factory.impls.mysql.DireccionDAO(dbType);        }
        return idao;
    }

    @Override
    public DBTypes getDriver() {
        return dbType;
    }

}

