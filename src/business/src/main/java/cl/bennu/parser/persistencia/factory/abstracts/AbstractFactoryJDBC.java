package cl.bennu.parser.persistencia.factory.abstracts;


import cl.bennu.parser.enums.DBTypes;

/**
 * Created by bennu on 12-08-14.
 */
public abstract class AbstractFactoryJDBC {
    private DBTypes dbType ;
    public AbstractFactoryJDBC() {
        dbType = getDriver();
    }

    public abstract DBTypes getDriver();
}
