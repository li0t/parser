package cl.bennu.parser.business.ifaces;

import cl.bennu.parser.dto.ComunaDTO;
import cl.bennu.parser.dto.DireccionDTO;
import cl.bennu.parser.dto.ProvinciaDTO;
import cl.bennu.parser.dto.RegionDTO;

import java.util.List;

/**
 * Created by bennu on 29-07-14.
 */
public interface ILocalidadBO {


    List<RegionDTO> getRegiones();

    List<ProvinciaDTO> getProvincias(int idRegion);

    List<ComunaDTO> getComunas(int idProvincia);

    List<DireccionDTO> getDirecciones(int rut, int com, int pro, int reg);
}
