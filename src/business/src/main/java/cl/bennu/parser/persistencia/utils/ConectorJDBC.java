package cl.bennu.parser.persistencia.utils;


import cl.bennu.parser.enums.DBTypes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Created by bennu on 13-08-14.
 */
public class ConectorJDBC {
    String host = "localhost";
    String usuario = "root";
    String pass = "bennu";
    String dBase = "db_bennu";

    Connection conexion;
    String error;

    public ConectorJDBC(String driver, DBTypes dbType) {

        try {
            Class.forName(driver);
            conexion = DriverManager.getConnection("jdbc:" + dbType.getLabel().toLowerCase()
                    + "://" + host + "/" + dBase, usuario, pass);

        } catch (SQLException e) {

            error = e.getMessage();

        } catch (ClassNotFoundException e) {

            error = e.getMessage();

        }

    }


    public Connection getConexion() {

        return conexion;

    }

    public void cierraConexion() {

        try {

            conexion.close();

        } catch (Exception ex) {

        }
    }


    public String getMensajeError() {

        return error;
    }


}