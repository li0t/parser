package cl.bennu.parser.persistencia.factory.impls.postgres;

import cl.bennu.parser.dto.ComunaDTO;
import cl.bennu.parser.enums.DBTypes;
import cl.bennu.parser.persistencia.factory.iface.IComunaDAO;
import cl.bennu.parser.persistencia.utils.ConectorJDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bennu on 12-08-14.
 */
public class ComunaDAO implements IComunaDAO {

    ConectorJDBC conector;
    Connection connection;
    PreparedStatement statement;
    ResultSet res;
    String consulta = "SELECT * FROM comuna where idProCom=?";


    public ComunaDAO(DBTypes dbType) {
        conector = new ConectorJDBC(get(), dbType);
        connection = conector.getConexion();
    }


    @Override
    public String get() {
        return "org.postgresql.Driver";
    }

    @Override
    public List<ComunaDTO> getComunas(int proId) {
        List<ComunaDTO> list = new ArrayList();

        try {
            statement = connection.prepareStatement(consulta);
            statement.setInt(1,proId);
            res = statement.executeQuery();
            while (res.next()) {
                ComunaDTO comunaDTO = new ComunaDTO();
                comunaDTO.setId(res.getInt(1));
                comunaDTO.setName(res.getString(2));
                list.add(comunaDTO);
            }
            res.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return list;
    }
}
