package cl.bennu.parser.persistencia.factory.iface;


import cl.bennu.parser.dto.RegionDTO;
import java.util.List;

/**
 * Created by bennu on 12-08-14.
 */
public interface IRegionDAO extends  ITypedDAO<String>{

    String get();

    List<RegionDTO> getRegiones();
}
