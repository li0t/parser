package cl.bennu.parser.persistencia.factory.iface;

import cl.bennu.parser.dto.ComunaDTO;
import java.util.List;

/**
 * Created by bennu on 12-08-14.
 */
public interface IComunaDAO extends ITypedDAO<String> {

    String get();

    List<ComunaDTO> getComunas(int proId);
}
