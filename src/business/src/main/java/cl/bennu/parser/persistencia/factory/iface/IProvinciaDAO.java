package cl.bennu.parser.persistencia.factory.iface;

import cl.bennu.parser.dto.ProvinciaDTO;
import java.util.List;

/**
 * Created by bennu on 25-08-14.
 */
public interface IProvinciaDAO {

    String get();

    List<ProvinciaDTO> getProvincias(int regId);
}
