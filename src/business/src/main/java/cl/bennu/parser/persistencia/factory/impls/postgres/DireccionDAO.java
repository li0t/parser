package cl.bennu.parser.persistencia.factory.impls.postgres;

import cl.bennu.parser.dto.DireccionDTO;
import cl.bennu.parser.enums.DBTypes;
import cl.bennu.parser.persistencia.factory.iface.IDireccionDAO;
import cl.bennu.parser.persistencia.utils.ConectorJDBC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bennu on 25-08-14.
 */
public class DireccionDAO  implements IDireccionDAO{
    ConectorJDBC conector;
    Connection connection;
    Statement statement;
    ResultSet res;
    String consulta = "SELECT * FROM direccion";


    public DireccionDAO(DBTypes dbType) {
        conector = new ConectorJDBC(get(),dbType);
        connection = conector.getConexion();
    }

    @Override
    public String get() {
        return "org.postgresql.Driver";
    }

    @Override
    public List<DireccionDTO> getDirecciones(Object form) {
        List<DireccionDTO> list = new ArrayList();

        try {
            statement = connection.createStatement();
            res = statement.executeQuery(consulta);
            while (res.next()) {
                DireccionDTO direccionDTO = new DireccionDTO();
                direccionDTO.setId(res.getInt(1));
                direccionDTO.setName(res.getString(2));
                list.add(direccionDTO);
            }
            res.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return list;
    }
}
